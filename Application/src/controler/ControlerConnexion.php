<?php

namespace sapnu\controler;

use \sapnu\models\Profil;
use sapnu\vue\VueCompte;
use sapnu\vue\VueConnexion;

class ControlerConnexion
{
    function connexion(){
        $vue = new VueConnexion();
        if(isset($_POST['mail'])&&isset($_POST['mdp'])){
            $prof=Profil::where('mail','=',$_POST['mail'])->first();
            if(isset($prof)){
                if(password_verify($_POST['mdp'],$prof->mdp)){
                    if(!isset($_SESSION)){
                        session_start();
                    }
                    $_SESSION['mail'] = $prof->mail;
                    $_SESSION['id'] = $prof->id;
                    $_SESSION['loggedin'] = true ;
                    $vue = new VueCompte();
                    echo $vue->menu();
                }else{
                    echo $vue->connexion(2);
                }
            }else{
                echo $vue->connexion(3);
            }
        }
    }

    function creation(){
        if(isset($_POST['nom'])&&isset($_POST['prenom'])&&isset($_POST['mail'])&&isset($_POST['mdp'])&&isset($_POST['C_mdp'])){
            $vue = new VueConnexion();
            $profils=Profil::all();
            $existe=false;
            foreach($profils as $u){
                if($_POST['mail']==$u->mail){
                    $existe=true;
                }
            }
            if(!$existe) {
                if ($_POST['mdp'] == $_POST['C_mdp']) {
                    $prof=new Profil();
                    $prof->nom=$_POST['nom'];
                    $prof->prenom=$_POST['prenom'];
                    $prof->mail=$_POST['mail'];
                    $prof->mdp=password_hash(filter_var($_POST['mdp'],FILTER_SANITIZE_STRING),PASSWORD_DEFAULT,[ 'cost' => 12]);
                    $prof->save();
                    echo $vue->connexion(1);
                }else{
                    echo $vue->creation(2);
                }
            }else{
                echo $vue->creation(3);
            }
        }
    }

    function deconnexion(){
<<<<<<< HEAD
        if(isset($_SESSION)){
            sesion_stop();
=======
        if (!isset($_SESSION)){
            session_start() ;
>>>>>>> d4228724a94fabc18ec4af125fee6c361077f341
        }
        unset($_SESSION['loggedin']) ;
        unset($_SESSION['mail']);
        unset($_SESSION['id']);
        session_destroy();

        $app = \Slim\Slim::getInstance();
        $app ->redirect($app->urlFor('home'));
    }

    function modifMail(){
        $vue = new VueCompte();
        $profils=Profil::all();
        $existe=false;
        foreach($profils as $u){
            if($_POST['mail']==$u->mail){
                $existe=true;
            }
        }
        if(!existe) {
            if (isset($_POST['mail'])) {
                if (isset($_SESSION)) {
                    $prof = Profil::find($_SESSION['id']);
                    $prof->mail = $_POST['mail'];
                    $prof->save();

                    echo "<p>mail modifié</p>".$vue->detailProfil();
                }
            }
        }else{
            echo "<p>Email déjà utilisé</p>".$vue->modif(1);
        }
    }

    function modifNom(){
        $vue = new VueCompte();
        if (isset($_POST['nom'])) {
            if (isset($_SESSION)) {
               $prof = Profil::find($_SESSION['id']);
               $prof->nom = $_POST['nom'];
               $prof->save();

               echo "<p>nom modifié</p>".$vue->detailProfil();
            }
        }
    }

    function modifPrenom(){
        $vue = new VueCompte();
        if (isset($_POST['prenom'])) {
            if (isset($_SESSION)) {
                $prof = Profil::find($_SESSION['id']);
                $prof->nom = $_POST['prenom'];
                $prof->save();

                echo "<p>prenom modifié</p>".$vue->detailProfil();
            }
        }
    }

    function modifMdp(){
        $vue = new VueCompte();
        if (isset($_POST['mdp'])&&isset($_POST['C_mdp'])) {
            if ($_POST['mdp'] == $_POST['C_mdp']){
                if (isset($_SESSION)) {
                    $prof = Profil::find($_SESSION['id']);
                    $prof->mdp = password_hash(filter_var($_POST['mdp'], FILTER_SANITIZE_STRING), PASSWORD_DEFAULT, ['cost' => 12]);
                    $prof->save();
                    echo "<p>Mot de passe modifié</p>" . $vue->detailProfil();
                }
            }else{
                echo "<p>Confirmation de mdp érroné</p>" . $vue->modif(4);
            }
        }
    }
}