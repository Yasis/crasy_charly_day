<?php
/**
 * Created by PhpStorm.
 * User: ulyss
 * Date: 07/02/2019
 * Time: 17:45
 */

namespace sapnu\vue;
use \sapnu\models\Profil;

class VueCompte extends Vue
{
    function menu(){
        $pre="";
        if(isset($_SESSION['id'])){
            $pro=Profil::find($_SESSION['id']);
            $pre=$pro->prenom;
        }

        $HTML = <<<END
         <!DOCTYPE html>
    <html>
        <head><center> <strong><font size="15">bienvenu $pre</font></strong></center></head>
        <body>
            
            <form id="offre" method="get" action="offres">

              <p>
                <button type=submit>Voir les offres</button>
              </p>
        
            </form>
            
            <form id="dep" method="get" action="deplacement">

              <p>
                <button type=submit>deplacement</button>
              </p>
        
            </form>
            
            <form id="profil" method="get" action="profil">

              <p>
                <button type=submit>détail sur votre profil</button>
              </p>
        
            </form>
            
            <form id="deconnex" method="get" action="deco">

              <p>
                <button type=submit>Déconnexion</button>
              </p>
        
            </form>
        </body>
    </html>
END;
        return self::buildHtml($HTML) ;
    }

    function detailProfil(){
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();

        $pre="";
        $nom="";
        $mail="";

        if(isset($_SESSION['id'])){
            $pro=Profil::find($_SESSION['id']);
            $pre=$pro->prenom;
            $nom=$pro->nom;
            $mail=$pro->mail;
        }

        $HTML = <<<END
         <!DOCTYPE html>
    <html>
        <head><center> <strong><font size="15">Détails de votre compte</font></strong></center></head>
        <body>
            
            <form id="offre" method="get" action="modifMail">

              <p>
                <button type=submit>Modifier mail</button>
              </p>
        
            </form>
            
            <form id="dep" method="get" action="modifNom">

              <p>
                <button type=submit>Modifier nom</button>
              </p>
        
            </form>
            
            <form id="profil" method="get" action="modifPrenom">

              <p>
                <button type=submit>Modifier prenom</button>
              </p>
        
            </form>
            
            <form id="deconnex" method="get" action="deco">

              <p>
                <button type=submit>Modifier Mot de passe</button>
              </p>
        
            </form>
            
            <a href=$url/connexionCompte>
					<p>
						Retour au menu
					</p>
				</a>
        </body>
    </html>
END;
        return self::buildHtml($HTML) ;
    }

    function modif($i){
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $head="";
        $content="";
        switch($i){
            case 1 : {
                $head="Modification de votre mail";
                $content=<<<END
                    <form id="deconnex" method="post" action="modifMail">
                    
                      <p>
				        <input type="text" name="mail" placeholder="Nouveau Email"required>
			          </p>
        
                      <p>
                        <button type=submit>Modifier</button>
                      </p>
                
                    </form>    
END;

                break;
            }
            case 2 : {
                $head="Modification de votre nom";
                $content=<<<END
                    <form id="deconnex" method="post" action="modifNom">
                    
                      <p>
				        <input type="text" name="nom" placeholder="Nouveau Nom"required>
			          </p>
        
                      <p>
                        <button type=submit>Modifier</button>
                      </p>
                
                    </form>    
END;
                break;
            }
            case 3 : {
                $head="Modification de votre prenom";
                $content=<<<END
                    <form id="deconnex" method="post" action="modifPrenom">
                    
                      <p>
				        <input type="text" name="prenom" placeholder="Nouveau Prenom"required>
			          </p>
        
                      <p>
                        <button type=submit>Modifier</button>
                      </p>
                
                    </form>    
END;
                break;
            }
            case 4 : {
                $head="Modification de votre Mot de passe";
                $content=<<<END
                    <form id="deconnex" method="post" action="modifMdp">
                    
                      <p>
				        <input type="password" name="mdp" placeholder="Nouveau Mot de passe"required>
			          </p>
			          
			           <p>
				        <input type="password" name="C_mdp" placeholder="Confirmer"required>
			          </p>
        
                      <p>
                        <button type=submit>Modifier</button>
                      </p>
                
                    </form>    
END;
                break;
            }
        }
        $HTML=<<<END
        <!DOCTYPE html>
    <html>
        <head><center> <strong><font size="15">$head</font></strong></center></head>
        <body>
            $content       
            <a href=$url/profil>
					<p>
						Retour au profil
					</p>
				</a>
        </body>
    </html>

END;
        return self::buildHtml($HTML) ;
    }
}