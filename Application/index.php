<?php
require 'vendor/autoload.php';

use Illuminate\Database\Capsule\Manager as DB;

use \sapnu\vue\VueConnexion;
use \sapnu\controler\ControlerConnexion;

$conf = parse_ini_file("src/conf/conf.ini");
$app=new \Slim\Slim();

$db = new DB();
$db->addConnection($conf);
$db->setAsGlobal();
$db->bootEloquent();

//Racine du projet. Affichage de la connexion.
$app->get('/',function(){
	$vue = new VueConnexion();
	echo $vue->connexion(1);
})->name("home");

$app->get('/connexion' , function() {
        $vue = new VueConnexion();
        echo $vue->connexion(1);
})->name("connect") ;

$app->post('/connexionCompte',function(){
    $c = new ControlerConnexion();
    $c->connexion();
})->name("connexion");

$app->get('/compte',function(){
    $c = new ControlerConnexion();
    $c->connexion();
})->name("profil");

$app->get('/creerCompte',function(){
    $vue = new VueConnexion();
    echo $vue->creation(1);
})->name("creerCompte");

$app->post('/creerCompte',function() {
    $c = new ControlerConnexion();
    $c->creation();
});

$app->get('/deco',function(){
    $c = new ControlerConnexion();
    $c->deconnexion();
})->name("deco");

$app->get('/profil',function(){
    $vue = new VueCompte();
    echo $vue->detailProfil();
});

$app->get('/modifMail',function(){
    $vue = new VueCompte();
    echo $vue->modif(1);
});

$app->post('/modifMail',function(){
    $c = new ControlerConnexion();
    $c->modifMail();
});

$app->get('/modifNom',function(){
    $vue = new VueCompte();
    echo $vue->modif(2);
});

$app->post('/modifNom',function(){
    $c = new ControlerConnexion();
    $c->modifNom();
});

$app->get('/modifPrenom',function(){
    $vue = new VueCompte();
    echo $vue->modif(3);
});

$app->post('/modifPrenom',function(){
    $c = new ControlerConnexion();
    $c->modifPrenom();
});

$app->get('/modifMdp',function(){
    $vue = new VueCompte();
    $vue->modif(4);
});

$app->post('/modifMdp',function(){
    $c = new ControlerConnexion();
    $c->modifMdp();
});



$app->get('/offres' , function() {
    $c = new \sapnu\controler\ControlerOffre() ;
    echo $c->seeAllOffer() ;
})->name('offres') ;

$app->get('/offre' , function() {
    $c = new \sapnu\controler\ControlerOffre() ;
    echo $c->formulNewOffre() ;
})->name('offre') ;

$app->get('/newOffre' , function() {
    $c = new \sapnu\controler\ControlerOffre() ;
    echo $c->formulNewOffre() ;
})->name('newOffre') ;

$app->run();