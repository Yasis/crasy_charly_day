<?php
/**
 * Created by PhpStorm.
 * User: claire
 * Date: 07/02/19
 * Time: 19:38
 */

namespace sapnu\models;


class Emplois extends \Illuminate\Database\Eloquent\Model
{

    protected $table = "emplois";
    protected $primaryKey = 'idE';
    public $timestamps = false;

}