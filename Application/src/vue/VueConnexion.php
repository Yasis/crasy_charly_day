<?php

namespace sapnu\vue;


class VueConnexion extends Vue
{
	function connexion($i){
		$app = \Slim\Slim::getInstance();
		$url = $app->request->getRootUri();
        $message_mdp="mot de passe";
        $message_log="Email";
        switch($i){
            case 1 : {
                break;
            }
            case 2 : {
                $message_mdp="Mot de passe invalide !!!";
                break;
            }
            case 3 : {
                $message_log="Login invalide !!!";
                break;
            }
        }
		$HTML = <<<END
		    <!DOCTYPE html>
    <html>

    <body><section>
	
	<div id="connexion" >  
		<form  method="post" action="connexionCompte">
            <h1>Connexion</h1><br>
            <br><br>
            <h2>Utilisez votre compte</h2><br><br><br><br>
                
				<label>Adresse Email : </label>
                <input type="text" name="pseudo"  placeholder="$message_log"required>
				
                <label>Mot de passe : </label>
                <input type="password" name="mdp"  placeholder="$message_mdp"required>
                

                <br><br><br><br><br>
                
				<button id="b_con" type="submit">Connexion</button><br>
		</form>
		
		 <form id="creerCompte" method="get" action="creerCompte">

      <p>Pas encore inscrit ? Cliquez ici : </p>
      <p><button type=submit>Créer un Compte</button>
      </p>

    </form>
		
        </div>
		</section>
    </body></html>	
END;

        return self::buildHtml($HTML) ;
	}


	function creation($i){
        $app = \Slim\Slim::getInstance();
        $url = $app->request->getRootUri();
        $message_mdp="mot de passe";
        $message_log="Email";
        switch($i){
            case 1 : {
                break;
            }
            case 2 : {
                $message_mdp="Les mot de passe ne sont pas identique";
                break;
            }
            case 3 : {
                $message_log="Email deja utilisé";
                break;
            }
        }
        $HTML = <<<END
        <!DOCTYPE html>
    <html>

        <body>
		<section>
			<div id="inscription">  
            <form method="post" action="creerCompte">
				<h1>Nouveau compte</h1><br>
					<br><br><br>
				<p>(*) : champs obligatoires.</p><br><br><br>
                
				<label id ="label1">Nom (*) :</label>
                <input type="text" name="pseudo"  placeholder="dupont"required>
				
				<label id ="label2">Prenom (*) :</label>
                <input type="password" name="mdp"  placeholder="didier">
				
			    <br><br><br><br>
                <label id ="label3">Email :</label>
                <input type="text" name="mail" placeholder="$message_log"required>
				
				<br><br><br><br>
                <label>Mot de passe :</label>
                <input type="password" name="mdp" placeholder="$message_mdp"required>
				
				<label id ="label4">Confirmation :</label>
                <input type="password" name="C_mdp" placeholder="Confirmer mot de passe" required>
                <br><br><br><br><br>

				
                    <button type="submit">Creer le compte</button><br>

            </form>
		  </div>
			
	</section>	
			
			
        </body>
    </html>	
END;

        return self::buildHtml($HTML) ;
    }
}

