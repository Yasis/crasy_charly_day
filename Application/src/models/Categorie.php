<?php
/**
 * Created by PhpStorm.
 * User: claire
 * Date: 07/02/19
 * Time: 19:34
 */

namespace sapnu\models;


class Categorie extends \Illuminate\Database\Eloquent\Model
{

    protected $table = "categorie";
    protected $primaryKey = 'idC';
    public $timestamps = false;

}