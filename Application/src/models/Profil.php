<?php
/**
 * Created by PhpStorm.
 * User: claire
 * Date: 07/02/19
 * Time: 16:00
 */

namespace sapnu\models;

class Profil extends \Illuminate\Database\Eloquent\Model
{

    protected $table = "profil";
    protected $primaryKey = 'id';
    public $timestamps = false;

}