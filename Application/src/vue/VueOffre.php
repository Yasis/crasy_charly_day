<?php
/**
 * Created by PhpStorm.
 * User: claire
 * Date: 07/02/19
 * Time: 20:21
 */

namespace sapnu\vue;


class VueOffre extends Vue
{
    function listeOfOffre($liste)
    {
        $app = \Slim\Slim::getInstance();
        $moreInfo = $app->urlFor('offre');
        $newOffre = $app->urlFor('newOffre');
        $s = "";

        if (count($liste) === 0) {
            $s = "<div> <p>Aucune offres actuellement</p> </div>";
        } else {
            foreach ($liste as $offre) {
                $s .= "
<div class='uneOffre'>
       <p class='title'>" . $offre['titre'] . "</p>
       <p class='descr'>" . $offre['descr'] . "</p>
       <p class='type'>" . $offre['type'] . "</p>
       <p class='date'>" . $offre['datePubli'] . "</p>
       
       <form method='get' action=$moreInfo>
            <button type=\"button\"> + </button>
       </form> 
</div>";
            }
        }

        $s .= "<form method='get' action=$newOffre>
            <button type=\"button\"> Ajouter une offre </button>
       </form>" ;

        return self::buildHtml($s) ;
    }
}