<?php
/**
 * Created by PhpStorm.
 * User: claire
 * Date: 07/02/19
 * Time: 16:48
 */

namespace sapnu\vue;


class Vue
{
    public static function buildHtml($contentPage)
    {
        if (!isset($_SESSION)){
            session_start() ;
        }

        $logged = false ;
        if (isset($_SESSION['loggedin'])) {
            $logged = $_SESSION['loggedin'];
        }

        if ($logged) {
            return self::build($contentPage , self::navLOGIN()) ;
        }
        else
        {
            return self::build($contentPage , self::navVisiteur()) ;
        }

    }



    private static function build($contentPage , $nav)
    {
        $app = \Slim\Slim::getInstance();
        $css = $app->request->getRootUri()."../../CSS/home.css";
        return <<< END
        <!DOCTYPE html>
        <html>
            <head> 
                <meta charset="utf-8">
                 <title>sapnu-puas</title>
                <link rel="stylesheet" href='$css'>
            </head>
                
            <body>
                $nav
                    
                $contentPage
            </body>
        </html>
END;
    }


// TO DO
    /**
     * Méthode générant la barre de navigation
     * @return string
     */
    private static function navVisiteur()
    {
        $app = \Slim\Slim::getInstance();


        $url = $app->urlFor("home") ;
        /** $url2 = $app->urlFor("déplacement") ; */
        $url1 = $app->urlFor("offres") ;
        $url3 = $app->urlFor("connect") ;
        $url4 = $app->urlFor("creerCompte");

        $url2 = "" ;

        return <<< HEAD
<nav>
    <ul>
    <li><a href="$url">Home</a></li>
    <li><a href="$url1">Offres</a></li>
    <li><a href="$url2">Déplacement</a></li>
    <li><a href="$url3">Log in</a></li>
    <li><a href="$url4">Inscription</a></li>
    </ul>
</nav>
HEAD;
    }


    private static function navLOGIN()
    {
        $app = \Slim\Slim::getInstance();


        $url = $app->urlFor("home") ;
        /** $url2 = $app->urlFor("déplacement") ; */
        $url1 = $app->urlFor("offres") ;
        $url3 = $app->urlFor("profil") ;
        $url4 = $app->urlFor("deco") ;

        $url2 = "" ;

        return <<< HEAD
<nav>
    <ul>
    <li><a href="$url">Home</a></li>
    <li><a href="$url1">Offres</a></li>
    <li><a href="$url2">Déplacement</a></li>
    <li><a href="$url3">Profil</a></li>
    <li><a href="$url4">Log out</a></li>
    </ul>
</nav>
HEAD;
    }


    public function notDone() {
        return self::buildHtml("<p> FONCTIONNALITE PAS ENCORE IMPLEMENTE </p>") ;
    }
}