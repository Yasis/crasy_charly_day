'use strict'

window.addEventListener("load",()=>{


	let recherche = document.querySelector("recherche");

	recherche.addEventListener("onkeydown", rechercheNav());


});

function rechercheNav() {
  let input, filter, table, tr, td, i ,td2, buttonEffet,buttonRequis;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  buttonEffet = document.getElementById("buttonEffet");
  buttonRequis = document.getElementById("buttonRequis");
  if(buttonEffet.checked){
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[0];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }
  }
}