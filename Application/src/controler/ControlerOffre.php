<?php
/**
 * Created by PhpStorm.
 * User: claire
 * Date: 07/02/19
 * Time: 17:53
 */

namespace sapnu\controler;
use \sapnu\models\Emplois;
use sapnu\vue\VueOffre;
use sapnu\vue\Vue;


class ControlerOffre
{
    function seeAllOffer()
    {
        $allOffer = Emplois::Select("*")->get() ;
        $vue = new VueOffre() ;
        return $vue->listeOfOffre($allOffer) ;
    }


    function formulNewOffre()
    {
        $vue = new Vue() ;
        return $vue->notDone() ;
    }
}